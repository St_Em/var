import requests
from bs4 import BeautifulSoup

def MainProgram():
	""" Main program that keeps things together
		WARNING: 	THE SCRIPT WILL ONLY WORK
					IF THE NAME OF THE MAIN DOMAIN
					THE SCRIPT WAS WRITTEN FOR
					WILL BE PROVIDED

					NOT TO EXHAUST THE DOMAIN
					HERE THE NAME WAS REPLACED
					WITH 'XXXXX'

					WILL BE PROVIDED ON A REQUEST
	"""

	searchingFor, link2find = GetName_ReturnLink()
	r = requests.get(link2find)
	rsoup = BeautifulSoup(r.content, 'lxml')
	
	if IfAnythingFound(rsoup):
		infs = GettingInformation(rsoup)
		for key in infs:
			if infs[key][2] == False:
				infs[key].append(GetNameOfBrandFromSingleLinks(infs[key][1]))
			elif infs[key][2] == True:
				infs[key].append(GetDetailsFromMultipleLinks(infs[key]))
		ModifyHowDoesItLookLike(infs, searchingFor)
	else:
		print ("Nie znaleziono nic dla \"{0}\"!".format(searchingFor))


def GetName_ReturnLink():
	""" Asks an user for a search query.

	args:
		USER INPUT 	(str), 	search query
	returns:
		tireName 	(str),	name of a query
		LINK 		(str),	full link for further processing
	"""

	tireName = input("Podaj nazwe opon, ktorych cene chcesz porownac: ")
	tireName2 = tireName.replace(" ", "+")
	return ( tireName, ("https://www.XXXXX.pl/Opony;szukaj-" + tireName2) )


def IfAnythingFound(link):
	""" Returns True if any record for a searched query is found

	args:
		link 		(str), 	full link
	returns:
		True/False 	(bool),	depends on whether a search query is found in the specific category name
	"""

	try:
		categoryName = link.find('div', {'class': "cat-nav__title__name"}).text.strip()
		return True if (categoryName == "Opony") else False
	except:
		return False


def GettingInformation(rsoup):
	""" Gets and store in a dictionary information about all of found products for a search query

	args:
		rsoup 		(), 	webpage content
	returns:
		infos 		(dict),	information about found products;
							returns also an information whether product is found
							in a single or multiple shops [required for further processing],
							infos[productName][multipleLinks]
	"""

	getAllFoundProd = rsoup.find_all('div', {'class': "cat-prod-row__body"})

	infos = {}
		
	for prod in getAllFoundProd:
		### This part search for additional information about a product
		try:
			infor = prod.find('ul', {'class': "prod-params cat-prod-row__params"}).get_text()
			infor = list(x for x in infor.split('\n') if len(x) > 0)
			if len(infor) == 0:
				infor = ["Brak dodatkowych informacji nt. produktu."]
		except:
			infor = ["Brak dodatkowych informacji nt. produktu."]

		### This part searches for a product price
		priceZloty = prod.find('span', {'class': "value"})
		priceGroszy = prod.find('span', {'class': "penny"})
		priceTotal = str(priceZloty.text) + str(priceGroszy.text)

		### This part searches for a further link (details) and name of a product
		findAclassWith_TitleAndLink = prod.find('a')
		productName = findAclassWith_TitleAndLink.get('title')
		link2FurtherDetails = "https://www.XXXXX.pl" + findAclassWith_TitleAndLink.get('href')

		### This part returns an information whether 
		### product is found in a single or multiple shops (required for further processing)
		if link2FurtherDetails.find("Click") == -1:
			multipleLinks = True
		else:
			multipleLinks = False
		infos[productName] = [priceTotal, link2FurtherDetails, multipleLinks, infor]

	return (infos)


def GetNameOfBrandFromSingleLinks(link):
	""" Gets the name of a domain where single links lead to

	args:
		link 		(str), 	a link if a product found only in one shop
	returns:
		name 		(str),	a reflink to an offer, useless actually;
							replaced with the main domain link later
	"""

	r = requests.get(link)
	rsoup = BeautifulSoup(r.content, 'lxml')
	rsoup = rsoup.text
	start = "window.location.replace(\""
	name = rsoup [rsoup.find(start)+(len(start)):rsoup.find(".pl", rsoup.find(start))+3]

	if len(name) == 0 or len(name) > 50:
		name = rsoup [rsoup.find(start)+(len(start)):rsoup.find(".com", rsoup.find(start))+4]

	return (name if (len(name) > 0 and len(name) < 50) else "Unknown external domain")


def GetDetailsFromMultipleLinks(dictWithDetFromMain):
	""" Browses dictionary from each returned item for a query

	args:
		dictWithDetFromMain (dict), a dictionary 
	returns:
		otherShopsDetails	(dict),	a dictionary containing for how much
									and where a product can be bought
	"""

	shopsName = []
	priceBasic = []
	priceWithDelivery = []

	link2multProducts = dictWithDetFromMain[1]
	r = requests.get(link2multProducts)
	rsoup = BeautifulSoup(r.content, 'lxml')

	### This part returns shops name 
	allSiteProdShop = rsoup.find_all('a', {'class': "store-logo go-to-shop "})
	for prod in allSiteProdShop:
		# print (prod)
		img = prod.find('img', alt=True)
		shopName = img['alt']
		shopsName.append(shopName)

	### This part returns prices in found shops
	allSiteProdPrice = rsoup.find_all('td', {'class': "cell-price"})
	for price in allSiteProdPrice:
		priceZloty = price.find('span', {'class': "value"})
		priceGroszy = price.find('span', {'class': "penny"})
		priceTotal = str(priceZloty.text) + str(priceGroszy.text)
		priceBasic.append(priceTotal)

		if ( price.find('span', {'class': "free-delivery-txt"}) ):
			priceWithDelivery.append(priceTotal)

		else:
			priceForDelievery = price.find('div', {'class': "product-delivery-info js_deliveryInfo"}).get_text().strip()
			priceForDelievery = priceForDelievery.replace("Z wysyłką od", "")
			priceForDelievery = priceForDelievery.replace("                                zł","")
			priceWithDelivery.append(priceForDelievery.strip())

	otherShopsDetails = {}

	for name, basic, delievery in zip(shopsName, priceBasic, priceWithDelivery):
		otherShopsDetails[name] = [basic, delievery]

	return (otherShopsDetails)


def ReturnAvgAndMaxPrice(file):
	""" Browses pandas rows and returns avg and max prices and number of shops an item can be bought

	args:
		file 					(df), pandas rows
	returns:
		avg, maxPrice, howMany	(str, str, int), avg and max prices, number of shops
	"""

	howMany = len(file.keys())
	total = 0
	maxPrice = 0

	for key in file:
		priceWithDel = file[key][1].replace(",", ".")
		try:
			total += float(priceWithDel)
		except ValueError:
			priceWithDel = file[key][0].replace(",", ".")
			total += float(priceWithDel)

		if maxPrice < float(priceWithDel):
			maxPrice = float(priceWithDel)

	avg = str(round(total/howMany, 2))
	avg = avg.replace(".", ",")
	maxPrice = str(maxPrice)
	maxPrice = maxPrice.replace(".", ",")

	return (avg, maxPrice, howMany)


def ModifyHowDoesItLookLike(infoFile, whatIsSearched):
	""" Modifies all the data gathered through the procedure

	args:
		infoFile 		(dict), dictionary containing all the data
		whatIsSearched 	(str), user inputted query to search
	returns:
		an Excel file with the arranged data gathered through a web search
	"""

	import pandas as pd

	df = pd.DataFrame(infoFile)
	df = df.T
	df.columns = ["Minimalna", "Link", "MultiLink", "Info", "Sklepy"]
	pd.set_option('display.max_columns', 8)

	### This part connects a list with information into a single row
	for element in range(len(df["Info"])):
		df["Info"][element] = (", ".join(df["Info"][element]))

	### This part arranges and updates data 
	### (depending on if item found in a single or multiple shops)
	for (index, row) in df.iterrows():
		if row["MultiLink"]:
			avg, maximal, stron = ReturnAvgAndMaxPrice(row["Sklepy"])
			df.loc[index, 'Srednia'] = avg
			df.loc[index, 'Maksymalna'] = maximal
			df.loc[index, 'Sklepow'] = stron
			df.loc[index, 'Sklepy'] = (", ".join(df.loc[index, 'Sklepy']))
		else:
			df.loc[index, 'Srednia'] = df.loc[index, 'Minimalna']
			df.loc[index, 'Maksymalna'] = "-"
			df.loc[index, 'Minimalna'] = "-"
			df.loc[index, 'Sklepow'] = "1"
			df.loc[index, 'Link'] = "www.XXXXX.pl"

	### Changing columns order
	df = df[ ['Sklepow', "Minimalna", "Srednia", "Maksymalna", "Info", "Link", "Sklepy"] ]

	whatIsSearched2 = whatIsSearched.replace("+", "_")
	whatIsSearched2 = whatIsSearched.replace(" ", "_")

	### Saving the output to an excel file
	searched = whatIsSearched2+".xls"
	df.to_excel(searched)
	print (f"Wynik wyszukiwania opony \"{whatIsSearched}\" zapisano do pliku o nazwie: {searched}")


MainProgram()