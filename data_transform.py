def MainProgram():
	""" Main program that keeps things together.
		To see how it works, please use the example file (DT_example.xls) provided.
		It is a script that transform SSR molecular data as shown below.

		Example:

		IN:
		izolat	s1	s1	s1	s2		s2		s3		s3	s3	s4	s4
		A1		1	2	2.4	3		4		5		6	0	7	8
		A2		11	12	0	14.0	15.0	16.0	17	0	19	20
		A3		21	22	0	24.0	25.0	26.0	27	0	29	30
		A4		31	32	0	34.0	35.0	36.0	37	38	39	40

		OUT:
			s1	s2	s3  s4
		A1	1	3	5   7
		A1	2	4	6   8
		A1  2.4 0   0   0
		A2	11	14	16	19
		A2	12	15	17	20
		A3	21	24	26	29
		A3	22	25	27	30
		A4	31	34	36	39
		A4	32	35	37	40
		A4	0	0	38	0
	"""

	import pandas as pd
	import numpy as np
	global pd, np

	dataLoaded_normal, dataLoaded_transposed = GettingTheData( getExcel() )
	markers, isolates = (MarkerAndIsolatesNames( dataLoaded_transposed ) )
	numbOfRowsForIsolate, wayToFillNewDf = (MaxNumberOfAllelesForEachIsolate_way2fill( dataLoaded_normal, markers ))
	indexes = (ListOfIndexNames(numbOfRowsForIsolate, isolates))
	newDf = (CreateNewDf(indexes, markers))
	newFilledDf = FillingTheNewDf(newDf, dataLoaded_normal, wayToFillNewDf, isolates, markers)
	### Saves the output to the file
	newFilledDf.to_excel("saved_df.xls")


def getExcel():
	""" Asking user to select a file to work on
	args:
		USER SELECTION 		(xls OR xlsx), 	a Excel file containing SSR data
	returns:
		import_file_path 	(str),			a path to a file
	"""

	from tkinter import filedialog
	import_file_path = filedialog.askopenfilename(title = "Select file containing SSR data", \
		filetypes = (("Excel 97-2003 file","*.xls"),("Excel file","*.xlsx"), ("all files","*.*")))
	return (import_file_path)


def GettingTheData(path):
	""" Loads the SSR data from the file provided into a pandas dataframe
	args:
		path 	(str), 	a path leading to the data
	returns:
		dataT 	(dataframe),	a dataframe cotaining the loaded and transposed SSR data
		data 	(dataframe),	a dataframe cotaining the loaded SSR data
	"""

	data = pd.read_excel(path, index_col=0, header=0)
	### Replacing missing values with 0s
	data[ data.isna() ] = 0		
	dataT = data.T

	return (data, dataT)


def MarkerAndIsolatesNames(data):
	""" Returns names of the markers
	args:
		data 	(dataframe),	a dataframe cotaining the loaded SSR data
	returns:
		markerNames (list), 	a list containing the names of the markes, just unique ones
		isolateNames (list), 	a list containing the names of the isolates
	"""

	markers = list(data.index)
	markerNames = [name for name in markers if name.find('.') == -1]

	isolateNames = list(data.columns)

	return (markerNames, isolateNames)


def MaxNumberOfAllelesForEachIsolate_way2fill(data, markerNames):
	""" Returns the number of maximum number of alleles for a given isolate
		that will be used further as a number of rows occupied per each isolate.
		Also returns the way to fill a new df.

		IN:
		isolate	s1	s1	s1	s2		s2		s3		s3	s3	s4	s4
		A1		1	2	2.4	3		4		5		6	0	7	8

		OUT:
		3, [3, 2, 2, 2], 		since s1 has three alleles and it is maximal value
								worth noting is that s3 also 'contains' three alleles, but one value is 0;
								a list that contains numbers of alleles per marker:
								3, because s1 contains alleles 1, 2 and 2.4
								2, because s2 contains alleles 3, 4 ...

	args:
		data 	(dataframe),		a dataframe cotaining the loaded SSR data
		markerNames (list), 		a list containing the names of the markes, just unique ones
	returns:
		rowsPerIsolate (list), 		a list containing the number of rows for each isolate the data will be filled to
		numbersOfAlleles (list),	a list with info how many alleles per marker per isolate are in original df
										used further to fill new df
	"""

	rowsPerIsolate = []
	numbersOfAlleles = []

	### Go through isolate names: A1, A2 ...
	for isolate in data.index:
		maximalRowNumb = 0
		iso_numberOfAlleles = []

		### Go through unique marker names: s1, s2 ...
		for i in range( len(markerNames) ):
			numbOfGivenMarker = 0
			marker_numberOfAlleles = 0

			### Go through columns containg all marker names: s1, s1.1, s1.2 ...
			for j in range( len(data.columns.values) ):

				### Check if marker name (s1) is the same as s1.1 AND it is not zero
				### If so, increment
				if data.columns.values[j].startswith(markerNames[i]) and data.loc[isolate][data.columns.values[j]] != 0:
					numbOfGivenMarker += 1
					marker_numberOfAlleles += 1

			iso_numberOfAlleles.append(marker_numberOfAlleles)

			### Check if the given markers has more alleles than a maximal one
			if numbOfGivenMarker > maximalRowNumb:
				maximalRowNumb = numbOfGivenMarker

		numbersOfAlleles.append(iso_numberOfAlleles)
		rowsPerIsolate.append(maximalRowNumb)

	return (rowsPerIsolate, numbersOfAlleles)


def ListOfIndexNames(rowsNumbs, isolateNames):
	""" Returns a list of of isolate names to be used as row indexes 

	args:
		rowsNumbs (list), 		a list containing the number of rows for each isolate the data will be filled to
		isolateNames (list), 	a list containing the names of the isolates
	returns:
		rowIndexes (list), 		a list with isolate names directly to be used as row indexes
	"""

	listNames = ( [ [isolateName]*rows for isolateName, rows in zip(isolateNames, rowsNumbs) ] )
	rowIndexes = [ item for sublist in listNames for item in sublist]

	return (rowIndexes)


def CreateNewDf(rowIndexes, markerNames):
	""" Prepares a new df of desired sizes

	args:
		rowIndexes (list), 		a list with isolate names directly to be used as row indexes
									and its length is desired as a df dimension
		markerNames (list), 	a list containing the unique names of the markes to be used as column names
									and its length is desired as a df dimension
	returns:
		newDf (dataframe), 		an empty dataframe of desired dimension for further processing
	"""

	zero_matrix = np.zeros( (len(rowIndexes), len(markerNames)) )
	newDf = pd.DataFrame(zero_matrix, columns=markerNames, index=rowIndexes)

	return (newDf)


def FillingTheNewDf(newDf, oldDf, howToFillDf, isolateNames, markers):
	""" Filling the new df with values from old df

	args:
		newDf (dataframe), 		an empty dataframe of desired dimension  
		oldDf 	(dataframe),	a dataframe cotaining the loaded SSR data
		howToFillDf (list),		a list with info how many alleles per marker per isolate are in original df
									used further to fill new df
		isolateNames (list), 	a list containing the names of the isolates
		markerNames (list), 	a list containing the unique names of the markes to be used as column names
	returns:
		newDf (dataframe), 		a filled final df
	"""

	### Browsing through each isolate data...
	for i, isolate in enumerate(isolateNames):
		start = 0

		### ... and updating new df with corresponding values
		for j in range( len(howToFillDf[i]) ):
			newDf.at[isolate, markers[j]][0:howToFillDf[i][j]] = oldDf.loc[isolate, oldDf.loc[isolate] > 0].iloc[start:start+howToFillDf[i][j]]
			start += howToFillDf[i][j]

	return (newDf)


MainProgram()