import pandas as pd
from tkinter import filedialog


def MainProgram():
	""" Main program that keeps things together 
		To see how it works, please use the example file (M_example.xlsx) provided.

		Example:
		
		INPUT:
		M P1a P1b   M   P2a P2b   M   K1a K1b K2a K2b
		A 1	  1     A   1   0     A   1   1   0   0   
		B 1   0     B   1   0     B   1   0   1   1
		C 1   1                   C   1   1   0   0

		RESULT:
           P1 	P2 	 none
		A  1    0    1 			(K1 from P1, K2 from none of parents)
		B  1    1    1 			(K1 from P1 and P1, K2 from none of parents)
		C  1    1    0 			(K1 from P1, K2 from P2 (no data for a parent equals 0, 0))
	"""

	path = (getExcel())
	fileData = pd.ExcelFile(path)

	for sheet_name in fileData.sheet_names:
		a, b, c = ReadingTheData(path, sheet_name)
		SavingToFile(sheet_name, CountingFRs(a, b, c))


def getExcel():
	""" Asking user to select a file to work on
	args:
		USER SELECTION 		(xls OR xlsx), 	a Excel file containing MSAP data
	returns:
		import_file_path 	(str),	a path to a file
	"""

	import_file_path = filedialog.askopenfilename(title = "Select file containing MSAP data", \
		filetypes = (("Excel file","*.xlsx"),("Excel 97-2003 file","*.xls"),("all files","*.*")))
	return (import_file_path)


def ReadingTheData(path, sheet_name):
	""" Browses through each sheet of a file and reads the data

	args:
		path 		(str), 	path to a file
		sheet_name 	(str), 	name of a sheet
	returns:
		firstParent, secondParent, kidds (dict, dict, dict), dictionaries containing data about
															 marker scores for both parents separately
															 and each of their progenies:
															 parents[marker] = [scores],
															 progenies[marker][progeny_numb] = [scores]
	"""

	firstParent = {}
	secondParent = {}
	kidds = {}
	fileData = pd.read_excel(path, header = 0, sheet_name=sheet_name)

	for element in fileData.index:

		### This part makes sure that there is data for markers in a row
		### Only progeny has data for all of the markers
		### If in parents column no data for a given marker is present
		### 	it means that the score for the markes is [0, 0]
		if (pd.notnull(fileData.iloc[element][0])):
			key1parent = str(int(fileData.iloc[element][0]))
			firstParent[key1parent] = [int(fileData.iloc[element][1]), int(fileData.iloc[element][2])]

		if (pd.notnull(fileData.iloc[element][3])):
			key2parent = str(int(fileData.iloc[element][3]))
			secondParent[key2parent] = [int(fileData.iloc[element][4]), int(fileData.iloc[element][5])]

		if (pd.notnull(fileData.iloc[element][6])):
			key3parent = str(int(fileData.iloc[element][6]))

			### This part counts the marker scores from the progeny
			kidds[key3parent] = {}
			count = 1
			which = 1
			kidds[key3parent][which] = []

			for i, kidPart in enumerate(fileData.iloc[element][7:]):
				if count == 1:
					kidds[key3parent][which].append(int(kidPart))
					count += 1
				elif count == 2:
					kidds[key3parent][which].append(int(kidPart))
					count = 1
					which += 1
					kidds[key3parent][which] = []

	return (firstParent, secondParent, kidds)


def CountingFRs(firstParent, secondParent, kidds):
	""" Tracking the progeny marker descent

	args:
		firstParent, secondParent, kidds 	(dict, dict, dict), dictionaries containing data about
															 	marker scores for both parents separately
															 	and each of their progenies:
															 	parents[marker] = [scores],
															 	progenies[marker][progeny_numb] = [scores]
	returns:
		markerLists 	(list),	a list containing name of a marker, followed by number of progeny
								that inherited marker read from [first parent, second parent, none of the parent]
	"""

	markerLists = []

	### If the marker read was not present in the parent data
	### 	it means that for given marker the read was [0, 0]
	for key in kidds:
		if key not in firstParent.keys():
			firstParent[key] = [0, 0]
		if key not in secondParent.keys():
			secondParent[key] = [0, 0]

		tempList = []
		FR1 = 0
		FR5 = 0
		zmienione = 0

		### This part compares the reads from progeny and from each of the parent
		for val in kidds[key]:
			if len(kidds[key][val]) > 0:
				if (kidds[key][val] != firstParent[key]) and (kidds[key][val] != secondParent[key]):
					zmienione += 1
				elif (kidds[key][val] == firstParent[key]) and (kidds[key][val] == secondParent[key]):
					FR1 += 1
					FR5 += 1
				elif (kidds[key][val] == firstParent[key]):
					FR1 += 1
				elif (kidds[key][val] == secondParent[key]):
					FR5 += 1

		tempList = [key, FR1, FR5, zmienione]
		markerLists.append(tempList)

	return (markerLists)


def SavingToFile(sheet, markerLists):
	""" Asks an user for a search query.

	args:
		markerLists (list), a list containing MSAP data
		sheet_name 	(str), 	name of a sheet
	returns:
		separate txt files named after the sheet the data was taken from
			containing the results saved to the path where script is placed
	"""

	outFile = sheet + "_msap.txt"
	with open(outFile, 'w') as f:
		for i in range(len(markerLists)):
			for x, element in enumerate(markerLists[i]):
				element = str(element)
				f.write(element)
				if x < (len(markerLists[i]) - 1):
					f.write("\t")
			f.write ("\n")


MainProgram()